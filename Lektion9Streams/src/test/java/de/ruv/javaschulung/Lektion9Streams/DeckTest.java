package de.ruv.javaschulung.Lektion9Streams;

import java.nio.BufferOverflowException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.ruv.javaschulung.Lektion9Streams.CardGame.Card;
import de.ruv.javaschulung.Lektion9Streams.CardGame.Deck;
import de.ruv.javaschulung.Lektion9Streams.CardGame.Suit;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class DeckTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public DeckTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( DeckTest.class );
    }

    
    private <T> Set<T> findDuplicates(Collection<T> collecton) {
    	Set<T> uniques = new HashSet<>();
    	return collecton.stream()
    			.filter( e -> !uniques.add(e) )
    			.collect(Collectors.toSet());
    }
    
    private Deck flipAll(Deck d) {
    	d.getCards().stream().filter( c -> !c.isFaceUp() ).forEach( c -> c.flip() );
    	return d;
    }
    
    /**
     * Rigourous Test :-)
     */
    public void testConstruct() {
    	Deck d1 = flipAll(Deck.empty());
    	assertTrue("Not an empty deck!", d1.getCards().isEmpty());
    	
    	Deck d2 = flipAll(Deck.standard());
    	assertEquals("A standard deck needs 52 cards", 52, d2.getCards().size());
    	
    	Consumer<Suit> con = (s) -> 
    		assertEquals(
    				"Suit " + s + "needs 13 cards",
    				13,
    				d2.getCards().stream().filter( c -> c.getSuit() == s).collect(Collectors.toList()).size()
    		);
    	
    	for (Suit s : Suit.values())
    		con.accept(s);
    	
    	assertEquals("A standard Dack has no duplicates", 0, findDuplicates(d2.getCards()).size());
    	
    	/*
    	DIAMOND	((char)9826), //9830
    	CLUB	((char)9827),
    	HEART	((char)9825), //9829
    	SPADE	((char)9824);*/
    }
    
    public void testShuffle() {
    	Deck d1 = flipAll(Deck.standard());
    	Deck d2 = flipAll(Deck.standard());
    	Deck d3 = flipAll(Deck.standard().shuffle());
    	
    	assertEquals(d1.getCards(), d2.getCards());
    	assertFalse(d1.equals(d3));
    	assertFalse(d1.equals(d2.shuffle()));
    	assertFalse(d2.equals(d3));
    	
    }
    
    public void testFilter() {
    	//int methods = int[] {Deck::diamonds, Deck::clubs, Deck::hearts, Deck::spades};
    	//int foo = Deck::diamonds;
    	Stream<Function<Deck, Deck>> methods = Stream.of( Deck::diamonds, Deck::clubs, Deck::hearts, Deck::spades );
    	
    	methods.forEach( m -> {
    		Deck d = m.apply(flipAll(Deck.standard()));
    		assertEquals("Filter for suit on standard deck should lead to 13 cards", 13 , d.getCards().size());
    		assertEquals("Deck should not have any duplicates", 0, findDuplicates(d.getCards()).size());
    	});
        	
    }
    
    public void testDraw() {
    	
    	Deck d1 = flipAll(Deck.standard().shuffle());
    	final int size = 52;
    	
    	for (int i = 0; i < size; i++) {
    		Card c = d1.getCards().get(0);
    		assertSame(c, d1.draw());
    		d1.getCards().stream().findFirst().ifPresent( n -> assertNotSame(c, n));
    	}
    	
    	for (int i = 0; i < size; i++) {
    		Deck d2 = flipAll(Deck.standard().shuffle());
    		
    		List<Card> expected = new LinkedList<>(); 
    		d2.getCards().subList(0, i).forEach( c -> expected.add(c) );
    		List<Card> actual = d2.draw(i, false);
    		assertEquals(expected, actual);
    	}
    	
    	Deck d3 = flipAll(Deck.standard().shuffle());
    	d3.draw(size);
    	
    	try {
			d3.draw();

		    fail( "Missing exception" );
		} catch( IndexOutOfBoundsException e ) {
			assertTrue(true);
		}
    	
    	for (int i = 0; i < size; i++) {
    		Deck d2 = flipAll(Deck.standard().shuffle());
    		
    		List<Card> expected = new LinkedList<>(); 
    		d2.getCards().subList(0, i).forEach( c -> expected.add(c) );
    		Collections.reverse(expected);
    		List<Card> actual = d2.draw(i);
    		assertEquals(expected, actual);
    	}
    }
    
    public void testDiscard() {
    	Deck d1 = flipAll(Deck.empty());
    	final int size = 52;
    	
    	for (int i = 0; i < size; i++) {
    		Deck d2 = flipAll(Deck.standard().shuffle());    		
    		Card c = d2.draw();
    		d1.discard(c);
    		
    		assertSame(c, d1.getCards().get(0));
    	}
    	
    	assertEquals(size, d1.getCards().size());
    	
    	for (int i = 0; i < size; i++) {
    		Deck d3 = flipAll(Deck.empty());
    		Deck d4 = flipAll(Deck.standard().shuffle());
    		
    		List<Card> expected = d4.draw(i);
    		d3.discard(expected);
    		Collections.reverse(expected);
    		assertEquals(expected, d3.getCards());
    	}
    }
    
    public void testPoints() {
    	Deck d1 = flipAll(Deck.standard());
    	
    	final int suits = 4;
    	final int cards = 13;
    	
    	List<Integer> expected = new LinkedList<>();
    	for (int i = 0; i < suits; i++) {
    		for (int j = 1; j <= cards; j++) {
    			expected.add(j);
    		}
    	}
    	
    	List<Integer> actual = d1.toPoints();
    	
    	assertEquals(expected, actual);
    	
    }
    
    public void testCount() {
    	Deck d1 = flipAll(Deck.standard().shuffle());    	
    	assertEquals(364, d1.count());
    	
    	Deck d2 = flipAll(Deck.standard().shuffle());    	
    	assertEquals(364, d2.count());
    }
    
    public void testFindFirst() {
    	Deck d1 = Deck.empty();
    	Deck d2 = flipAll(Deck.standard().shuffle());
    	
    	final int size = 52;
    	
    	for (int i = 0; i < size; i++) {
    		d2.getCards().stream()
    			.findFirst()
    			.ifPresent( c -> d2.findFirst().ifPresent( n -> assertSame(c, n) ));
    		Card c = d2.draw();
    		d1.discard(c);
    		d1.findFirst().ifPresent( n -> assertSame(c, n) );
    	}
    	
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
