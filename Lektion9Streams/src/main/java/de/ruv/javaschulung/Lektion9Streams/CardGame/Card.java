package de.ruv.javaschulung.Lektion9Streams.CardGame;

public class Card {
	Suit suit;
	Value value;
	boolean faceUp;
	
	public Card(Suit suit, Value value) {
		this.suit = suit;
		this.value = value;
	}
		
	public Suit getSuit() {
		return suit;
	}
	
	public Value getValue() {
		return value;
	}
	
	public boolean isFaceUp() {
		return faceUp;
	}
	
	public void flip() {
		this.faceUp = !this.faceUp;
	}
	
	@Override
	public String toString() {
		return (!this.isFaceUp())? "[ ]" : this.suit + "" + this.value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (faceUp ? 1231 : 1237);
		result = prime * result + ((suit == null) ? 0 : suit.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (faceUp != other.faceUp)
			return false;
		if (suit != other.suit)
			return false;
		if (value != other.value)
			return false;
		return true;
	}
	
	
}
