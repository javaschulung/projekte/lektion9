package de.ruv.javaschulung.Lektion9Streams.CardGame;

public enum Value {
	ACE		(1, "A"),
	TWO		(2, "2"),
	THREE	(3, "3"),
	FOUR	(4, "4"),
	FIVE	(5, "5"),
	SIX		(6, "6"),
	SEVEN	(7, "7"),
	EIGHT	(8, "8"),
	NINE	(9, "9"),
	TEN		(10, "10"),
	JACK	(11, "J"),
	QUEEN	(12, "Q"),
	KING	(13, "K");
	
	int points;
	String text;
	
	Value(int points, String text) {
		this.points = points;
		this.text = text;
	}
		
	public int getPoints() {
		return points;
	}
	
	public String getText() {
		return text;
	}
	
	@Override
	public String toString() {
		return  this.text + ((this.text.length() < 2)? " " : "");
	}
}
