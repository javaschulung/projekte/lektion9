package de.ruv.javaschulung.Lektion9Streams.CardGame;

public enum Suit {
	DIAMOND	((char)9826), //9830
	CLUB	((char)9827),
	HEART	((char)9825), //9829
	SPADE	((char)9824);
	
	char symbol;
	
	Suit(char symbol) {
		this.symbol = symbol;
	}
	
	public boolean isRed() {
		return this == DIAMOND || this == HEART;
	}
	
	public boolean isBlack() {
		return this == CLUB || this == SPADE;
	}
	
	@Override
	public String toString() {
		return Character.toString(this.symbol);
	}
}
