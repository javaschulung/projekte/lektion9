package de.ruv.javaschulung.Lektion9Streams.Klondike;

import java.util.List;

public class Main {
	public static void main(String[] args) {
		PlayGround playGround = new PlayGround();
		
		while (!playGround.turn());
		// oder auch:
		/*while (!playGround.hasWon()) {
			playGround.turn();
		}*/
		
	}
}
