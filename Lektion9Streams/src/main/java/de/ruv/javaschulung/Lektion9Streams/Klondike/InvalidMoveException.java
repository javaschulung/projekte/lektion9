package de.ruv.javaschulung.Lektion9Streams.Klondike;

public class InvalidMoveException extends Exception {

	public InvalidMoveException() {
		super("Invalid move");
	}
	
}
